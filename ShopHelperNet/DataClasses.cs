using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;


namespace ShopHelperNet
{
    [DataContract]
    public class Category
    {

        [DataMember]
        public DateTime created_at;

        [DataMember]
        public int id;

        [DataMember(Name = "parent-id")]
        public int parent_id;

        [DataMember]
        public int position;

        [DataMember]
        public string title;

        [DataMember]
        public DateTime updated_at;

    }

    [DataContract]
    public class Variants_Attribute
    {
        [DataMember]
        public decimal? price;

        [DataMember]
        public decimal? cost_price;

        [DataMember]
        public decimal? old_price;

        [DataMember]
        public string sku;

        [DataMember(EmitDefaultValue = false)]
        public int id;

        [DataMember]
        public DateTime updated_at;

    }




    [DataContract]
    public class Product
    {
        [DataMember(EmitDefaultValue = false)]
        public int id;

        [DataMember]
        public int category_id;

        string FTitle; 

        [DataMember]
        public string title { get { if (FTitle != null) return FTitle; else return ""; }
            set { FTitle = value; } }

        string FDescription;
        [DataMember]
        public string description
        {
            get { if (FDescription != null) return FDescription; else return ""; }
            set { FDescription = value; }
        }

        [DataMember]
        public string short_description;

        [DataMember]
        public string permalink;


        [DataMember(Name = "variants_attributes")]
        List<Variants_Attribute> variants_attributes
        { get { return variants; } }

        [DataMember(Name = "variants", EmitDefaultValue = false)]
        List<Variants_Attribute> variants_for_create
        { get { if (id == 0) return null; else return variants; } set { variants = value; } }

        public List<Variants_Attribute> variants;

        [DataMember]
        public DateTime updated_at;
        public Product()
        {
            variants = new List<Variants_Attribute>();
        }

    }



    [DataContract]
    public class ProductWrapper
    {
        [DataMember]
        public Product product;
    }

    [DataContract]
    public class VariantForUpdate
    {
        [DataMember]
        public int id;

        [DataMember]
        public decimal price;

        [DataMember]
        public decimal quantity;
    }

    [DataContract]
    public class VariantForUpdateWrapper
    {
        
        [DataMember]
        public VariantForUpdate variant;
    }

    [DataContract]
    public class VariantForUpdateGroup
    {
        [DataMember]
        public List<VariantForUpdate> variants;


    }


    [DataContract]
    public class PutResult
    {
        [DataMember]
        public int id;
                
        [DataMember]
        public string status;
    }

  


}
