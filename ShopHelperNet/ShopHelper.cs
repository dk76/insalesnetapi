using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

using Newtonsoft.Json;




namespace ShopHelperNet
{
    static class CommandsList
    {
        static public string cCategories = "categories.json";
        static public string cAddProduct = "products.json";
        static public string cGetProducts = "products.json";
        static public string cGetProductById = "products/{0}.json";
        static public string cModifyVariant = "products/{0}/variants/{1}.json";
        static public string cModifyGroupVariant = "products/variants_group_update.json";
    }

    public class ShopHelper
    {

        string FUrl, FUser, FPassword, FAccept, FContentType, FProtocol;

        public ShopHelper(string url, string user, string password, string protocol = "https")
        {
            FUrl = url;
            FUser = user;
            FPassword = password;
            FAccept = "application/json";
            FContentType = "application/json";
            FProtocol = protocol;
        }

        string getAuth()
        {
            return System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("UTF-8").GetBytes(FUser + ":" + FPassword));
        }



        string getRequest(string command)
        {
            HttpWebRequest request = (HttpWebRequest)getCommandRequest(command);

            request.Method = "GET";
            request.Accept = FAccept;
            request.ContentType = FContentType;
            request.Headers["Authorization"] = "Basic " + getAuth();

            HttpWebResponse response = (HttpWebResponse)request.GetResponseAsync().Result;
            StreamReader reader = new StreamReader(response.GetResponseStream());
            StringBuilder output = new StringBuilder();
            output.Append(reader.ReadToEnd());
            return output.ToString();

        }

        string postRequest(string command, byte[] data)
        {
            HttpWebRequest request = (HttpWebRequest)getCommandRequest(command);
            request.ContentType = FContentType;
            request.Method = "POST";
            request.Headers["Authorization"] = "Basic " + getAuth();
            request.Accept = FAccept;
            request.ContentLength = data.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(data, 0, data.Length);
            dataStream.Close();
            WebResponse response = request.GetResponseAsync().Result;
            StreamReader reader = new StreamReader(response.GetResponseStream());
            StringBuilder output = new StringBuilder();
            output.Append(reader.ReadToEnd());
            response.Close();
            return output.ToString();
        }


        string putRequest(string command, byte[] data)
        {
            HttpWebRequest request = (HttpWebRequest)getCommandRequest(command);
            request.ContentType = FContentType;
            request.Method = "PUT";
            request.Headers["Authorization"] = "Basic " + getAuth();
            request.Accept = FAccept;
            request.ContentLength = data.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(data, 0, data.Length);
            dataStream.Close();
            WebResponse response = request.GetResponseAsync().Result;
            StreamReader reader = new StreamReader(response.GetResponseStream());
            StringBuilder output = new StringBuilder();
            output.Append(reader.ReadToEnd());
            response.Close();
            return output.ToString();
        }
        private WebRequest getCommandRequest(string command)
        {
            return WebRequest.Create(FProtocol + "://" + FUrl + "/" + command);
        }

        static MemoryStream GenerateStreamFromString(string value)
        {
            return new MemoryStream(Encoding.UTF8.GetBytes(value ?? ""));
        }

        public List<Category> getCategories()
        {
            string s = getRequest(CommandsList.cCategories);
            return JsonConvert.DeserializeObject<List<Category>>(s);            
        }

        public Product AddProduct(Product product)
        {
            ProductWrapper pw = new ProductWrapper();
            pw.product = product;
            MemoryStream stream = new MemoryStream();
            string res= JsonConvert.SerializeObject(pw);
            byte[] data = System.Text.Encoding.GetEncoding("UTF-8").GetBytes(res);
            res = postRequest(CommandsList.cAddProduct, data);
            return JsonConvert.DeserializeObject<Product>(res);
        }

        public VariantForUpdate ModifyVariant(int product_id, int variant_id, VariantForUpdate v)
        {
            VariantForUpdateWrapper w = new VariantForUpdateWrapper();
            w.variant = v;
            v.id = variant_id;                     
            string res = JsonConvert.SerializeObject(w);
            byte[] data = System.Text.Encoding.GetEncoding("UTF-8").GetBytes(res);
            res = putRequest(string.Format(CommandsList.cModifyVariant, product_id, variant_id), data);
            return JsonConvert.DeserializeObject<VariantForUpdate>(res);
    }

        public List<PutResult> ModifyVariantGroup(VariantForUpdateGroup v)
        {
            string res = JsonConvert.SerializeObject(v);
            byte[] data = System.Text.Encoding.GetEncoding("UTF-8").GetBytes(res);
            res = putRequest(CommandsList.cModifyGroupVariant, data);
            return JsonConvert.DeserializeObject<List<PutResult>>(res);       
        }

    
        public List<Product> getProducts(int Page = 0, int perPage = 0)
        {
            string parameters = Page > 0 ? string.Format("page={0}", Page) : "";
            parameters += (parameters != "" ? "&" : "") + (perPage > 0 ? string.Format("per_page={0}", perPage) : "");
            string req = (CommandsList.cGetProducts + (parameters != "" ? "?" + parameters : ""));
            string s = getRequest(req);
            return JsonConvert.DeserializeObject<List<Product>>(s);
        }

        public Product getProduct(int id)
        {
            string req = string.Format(CommandsList.cGetProductById, id);
            string s = getRequest(req);            
            return JsonConvert.DeserializeObject<Product>(s);           
        }

    }
}
