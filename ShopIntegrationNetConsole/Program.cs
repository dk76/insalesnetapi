using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShopHelperNet;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;

namespace ShopIntegrationNetConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            ShopHelper helper = new ShopHelper("https://myshop-##myshop##.myinsales.ru/admin", "##login##", "##password##");
            List<Category> l = helper.getCategories();
            Product p = new Product();
            if ((l != null) && (l.Count > 0))
                p.category_id = l[0].id;
            else
                p.category_id = 0;
            p.title = "CSharp product";
            Variants_Attribute v = new Variants_Attribute();
            v.price = 100;
            v.sku = "12345567";
            p.variants.Add(v);
            p = helper.AddProduct(p);
            Console.ReadLine();
        }
    }
}
